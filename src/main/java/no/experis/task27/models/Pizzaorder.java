package no.experis.task27.models;

import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Pizzaorder {
    //ArrayList<PizzaItem> pizzaList = new ArrayList<PizzaItem>();
    int customerId;
    int id;
    String createdDate, submittedDate;

    public Pizzaorder(int _id, int customerId){
        this.customerId = customerId;
        this.id = _id;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = DateUtils.addHours(new Date(), 1);
        this.createdDate = dateFormat.format(date);
        this.submittedDate = "Not yet submitted";
    }

    public Pizzaorder(int id, int customerId,  String createdDate, String submittedDate) {
        this.customerId = customerId;
        this.id = id;
        this.createdDate = createdDate;
        this.submittedDate = submittedDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = DateUtils.addHours(new Date(), 1);
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        this.submittedDate = dateFormat.format(date);
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setId(int id) {
        this.id = id;
    }


}
