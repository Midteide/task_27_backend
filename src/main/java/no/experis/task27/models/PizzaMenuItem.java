package no.experis.task27.models;

public class PizzaMenuItem {
    String pizzaname, description;
    int id;
    String img;
    double ec=3.99, em=4.99, lg=5.99, price=7.99;

    public PizzaMenuItem(int id, String pizzaname, String description) {
        this.pizzaname = pizzaname;
        this.description = description;
        this.id = id;
    }


    public double getEc() {
        return ec;
    }

    public void setEc(double ec) {
        this.ec = ec;
    }

    public double getEm() {
        return em;
    }

    public void setEm(double em) {
        this.em = em;
    }

    public double getLg() {
        return lg;
    }

    public void setLg(double lg) {
        this.lg = lg;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public PizzaMenuItem(int id, String pizzaname, String description, String img) {
        this.pizzaname = pizzaname;
        this.description = description;
        this.id = id;
        this.img = img;
    }

    public PizzaMenuItem(int id, String pizzaname, String description, String img,double price) {
        this.pizzaname = pizzaname;
        this.description = description;
        this.id = id;
        this.img = img;
        this.price = price;
    }


    public String getPizzaname() {
        return pizzaname;
    }

    public void setPizzaname(String pizzaname) {
        this.pizzaname = pizzaname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return pizzaname;
    }

    public void setName(String pizzaname) {
        this.pizzaname = pizzaname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
