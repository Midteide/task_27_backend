package no.experis.task27.models;

import java.util.ArrayList;

public class OrderInformation {
    private Customer customer;
    private Pizzaorder order;
    private ArrayList<Pizza> pizzas;

    public OrderInformation(Customer customer, Pizzaorder order, ArrayList<Pizza> pizzas) {
        this.customer = customer;
        this.order = order;
        this.pizzas = pizzas;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Pizzaorder getOrder() {
        return order;
    }

    public void setOrder(Pizzaorder order) {
        this.order = order;
    }

    public ArrayList<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(ArrayList<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public double getPrice(){
        double returnPrice=0;
        for (Pizza p : this.pizzas){
            returnPrice += p.getPrice();
        }
        return returnPrice;
    }


/*
    @Override
    public String toString() {
        return "PersonInformation{" +
                "person=" + person +
                ", family=" + family +
                ", emails=" + emails +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    } */
}
