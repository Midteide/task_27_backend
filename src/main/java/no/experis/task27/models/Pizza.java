package no.experis.task27.models;

public class Pizza {
    boolean isLarge, extracheese, extrameat;
    String pizzaname, description;
    int id, orderId;

    public Pizza(int id, int orderId, boolean isLarge, boolean extracheese, boolean extrameat, String pizzaname, String description) {
        this.isLarge = isLarge;
        this.extracheese = extracheese;
        this.extrameat = extrameat;
        this.pizzaname = pizzaname;
        this.description = description;
        this.id = id;
        this.orderId = orderId;
    }
    public double getPrice() {
        double returnPrice=5.99;
        if (isLarge) returnPrice += 4.0;
        if (extracheese) returnPrice += 2.5;
        if (extrameat) returnPrice += 3.5;
         return returnPrice;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isLarge() {
        return isLarge;
    }

    public void setLarge(boolean large) {
        isLarge = large;
    }

    public String getPizzaname() {
        return pizzaname;
    }

    public void setPizzaname(String pizzaname) {
        this.pizzaname = pizzaname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return pizzaname;
    }

    public void setName(String pizzaname) {
        this.pizzaname = pizzaname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isExtracheese() {
        return extracheese;
    }

    public void setExtracheese(boolean extracheese) {
        this.extracheese = extracheese;
    }

    public boolean isExtrameat() {
        return extrameat;
    }

    public void setExtrameat(boolean extrameat) {
        this.extrameat = extrameat;
    }
}
