package no.experis.task27;

import no.experis.task27.models.Pizza;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//@RestController
public class Task27Application {

	public static void main(String[] args) {


		SpringApplication.run(Task27Application.class, args);
	}

}