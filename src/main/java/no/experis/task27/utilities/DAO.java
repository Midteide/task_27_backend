package no.experis.task27.utilities;


import no.experis.task27.models.*;

import java.sql.*;
import java.util.ArrayList;

public class DAO {

    //private String URI = "jdbc:postgresql://localhost:5432/postgres";
    //private String URI = "jdbc:postgresql://ec2-54-75-231-215.eu-west-1.compute.amazonaws.com:5432/dqal13he0175m?user=cefubhzlxtrvrj&password=455cbbbfaec65bf9da327ab52cde1d2bd189b4d5718d592655de6b9aca467d3f&sslmode=require";
    private String URI = "jdbc:postgresql://192.168.192:5432/postgres?user=intercom&password=623Eaygg";
    public Connection conn = null;
    public ArrayList<PizzaMenuItem> pizzaMenu = new ArrayList<PizzaMenuItem>();
    int pizzaId=1;

    public DAO() throws SQLException {
        // Adding some pizzas to the menu
        pizzaMenu.add(new PizzaMenuItem(pizzaId++, "Hawaii","Ham, Pineapple, Cheese","http://www.dominos.no/gallery/fmobile/302medium.png", 6.99));
        pizzaMenu.add(new PizzaMenuItem(pizzaId++,"Margharita", "Cheese, Tomato sauce", "http://www.dominos.no/gallery/fmobile/297medium.png", 4.99));
        pizzaMenu.add(new PizzaMenuItem(pizzaId++,"Hamorama", "Cheese, Ham, Mushroom","http://www.dominos.no/gallery/fmobile/413medium.png",5.49));
        pizzaMenu.add(new PizzaMenuItem(pizzaId++,"Hot Pepperoni", "Pepperoni, Mozarella, Jalapenos","http://www.dominos.no/gallery/fmobile/414medium.png",7.99));
        pizzaMenu.add(new PizzaMenuItem(pizzaId++,"Veggie Supreme", "Tomato sauce, Onion, Mushrooms","http://www.dominos.no/gallery/fmobile/310medium.png",5.99));



        //Make sure that we get the correct database
        if (System.getenv("JDBC_DATABASE_URL") != null) {
            URI = System.getenv("JDBC_DATABASE_URL");
        }
        this.openConn();
        System.out.println("Connection established...");

        // Create tables if not existing already
        this.initDatabase();
        System.out.println("Initialization complete!");
    }

    public int deletePizzaById(int id) {
        try {
            String insert = "DELETE FROM pizza WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int deleteOrderById(int id) {
        try {
            String insert = "DELETE FROM pizzaorder WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    //    public Customer(int id, String first_name, String last_name, String address) {
    public Customer getCustomerById(int id) {
        try {
            String insert = "SELECT * FROM customer where id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Customer(
                        result.getInt("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("address"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //    public Pizzaorder(int customerId, int id, String createdDate, String submittedDate) {
    public Pizzaorder getOrderById(int id) {
        try {
            String insert = "SELECT * FROM pizzaorder where id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Pizzaorder(
                        result.getInt("id"),
                        result.getInt("customerID"),
                        result.getString("createddate"),
                        result.getString("submitteddate"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Pizza getPizzaById(int id) {
        try {
            String insert = "SELECT * FROM pizza where id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Pizza(result.getInt("id"), result.getInt("orderID"), result.getBoolean("extracheese"), result.getBoolean("extrameat"), result.getBoolean("islarge"), result.getString("pizzaname"), result.getString("description"));

            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }
    public int addCustomer(String first_name, String last_name, String address) {
        try {

            //Creates a query for inserting a new row(person) to person table
            String insert = "INSERT INTO customer (first_name, last_name, address) VALUES (?,?,?) RETURNING id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, first_name);
            preparedStatement.setString(2, last_name);
            preparedStatement.setString(3, address);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                //Get the id of the newly added customer
                int id = result.getInt(1);
                //Add entries that are dependent existing.
                return id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public int addPizzaorder(int customerId) {
        try {
            Pizzaorder p = new Pizzaorder(999,customerId);

            String insert = "INSERT INTO pizzaorder (customerID, createddate, submitteddate) VALUES (?,?,?) RETURNING id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, p.getCustomerId());
            preparedStatement.setString(2, p.getCreatedDate());
            preparedStatement.setString(3, p.getSubmittedDate());
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                //Get the id of the newly added pizza
                int id = result.getInt(1);
                //Add entries that are dependent existing.
                return id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    // Setting the submit timestamp to order
    public int submitOrder(int orderId) {
        try {
            Pizzaorder p = getOrderById(orderId);
            p.setSubmittedDate();
            String insert = "UPDATE pizzaorder SET submitteddate=? WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, p.getSubmittedDate());
            preparedStatement.setInt(2, orderId);

            preparedStatement.executeUpdate();
            return orderId;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    //    public OrderInformation(Customer customer, Pizzaorder order, ArrayList<Pizza> pizzas) {
    public OrderInformation getOrderInformation(int orderID) {
        Pizzaorder p = getOrderById(orderID);
        return new OrderInformation(getCustomerById(p.getCustomerId()), p, getPizzasByOrderId(p.getId()));
    }

    public ArrayList<Pizzaorder> getAllOrders() {
        ArrayList<Pizzaorder> orderList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM pizzaorder ORDER  BY id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                orderList.add(
                        new Pizzaorder(
                                resultSet.getInt("id"),
                                resultSet.getInt("customerID"),
                                resultSet.getString("createddate"),
                                resultSet.getString("submitteddate")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderList;
    }

    public ArrayList<Pizza> getAllPizzas() {
        ArrayList<Pizza> pizzaList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM pizza ORDER  BY id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println("getAllPizza " + resultSet.getInt("orderID"));
                pizzaList.add(
// public Pizza(int id, int orderId, boolean isLarge, boolean extraCheese, boolean extraMeat, String pizzaname, String description) {
                        new Pizza(
                                resultSet.getInt("id"),
                                resultSet.getInt("orderID"),
                                resultSet.getBoolean("islarge"),
                                resultSet.getBoolean("extraCheese"),
                                resultSet.getBoolean("extraMeat"),
                                resultSet.getString("pizzaname"),
                                resultSet.getString("description")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pizzaList;
    }


    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customerList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM customer ORDER  BY id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerList.add(
                        new Customer(
                                resultSet.getInt("id"),
                                resultSet.getString("first_name"),
                                resultSet.getString("last_name"),
                                resultSet.getString("address")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }


    //    public Pizza(int id, boolean isLarge, String pizzaname, String description) {
    public ArrayList<Pizza> getPizzasByOrderId(int orderId) {
        ArrayList<Pizza> pizzaList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM pizza WHERE orderId=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, orderId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                pizzaList.add(new Pizza(result.getInt("id"),result.getInt("orderID"), result.getBoolean("extracheese"),result.getBoolean("extrameat"),result.getBoolean("islarge"), result.getString("pizzaname"), result.getString("description")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pizzaList;
    }


    // public Pizza(int id, int orderId, boolean isLarge, boolean extraCheese, boolean extraMeat, String pizzaname, String description) {
    public int addPizza(int orderId, int pizzaId, boolean isLarge, boolean extraCheese, boolean extraMeat) {
        try {

            String insert = "INSERT INTO pizza (orderID, islarge, extracheese, extrameat, pizzaname, description) VALUES (?,?,?,?,?,?) RETURNING id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, orderId);
            preparedStatement.setBoolean(2, isLarge);
            preparedStatement.setBoolean(3, extraCheese);
            preparedStatement.setBoolean(4, extraMeat);
            preparedStatement.setString(5, pizzaMenu.get(pizzaId).getPizzaname());
            preparedStatement.setString(6, pizzaMenu.get(pizzaId).getDescription());
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                //Get the id of the newly added pizza
                int id = result.getInt(1);
                //Add entries that are dependent existing.
                return id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }



    /**
     * Opens connection to database
     */
    public void openConn() {
        try {
            this.conn = DriverManager.getConnection(URI);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void initDatabase() {
        try {
            // CUSTOMER TABLE
            PreparedStatement customerPreparedStatement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS customer(" +
                            "id SERIAL PRIMARY KEY, " +
                            "first_name varchar, " +
                            "last_name varchar, " +
                            "address varchar, " +
                            "orders int references customer(id) ON DELETE SET NULL)");
            customerPreparedStatement.execute();
            // ORDERS TABLE
            PreparedStatement orderPreparedStatement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS pizzaorder(" +
                            "id SERIAL PRIMARY KEY, " +
                            "createddate varchar, " +
                            "submitteddate varchar, " +
                            "customerID int REFERENCES customer(id) ON DELETE CASCADE )");
            orderPreparedStatement.execute();
            // PIZZAS TABLE
            PreparedStatement pizzaPreparedStatement = conn.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS pizza(" +
                            "id SERIAL PRIMARY KEY, " +
                            "islarge varchar, " +
                            "extracheese varchar, " +
                            "extrameat varchar, " +
                            "pizzaname varchar, " +
                            "description varchar, " +
                            "orderID int REFERENCES pizzaorder(id) ON DELETE CASCADE)");
            pizzaPreparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<PizzaMenuItem> getMenu() {
        return pizzaMenu;
    }

}
