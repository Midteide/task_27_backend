package no.experis.task27.controllers;



import no.experis.task27.models.*;
import no.experis.task27.utilities.DAO;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;

@CrossOrigin
@RestController
public class OrderController {

    DAO dao = new DAO();

    public OrderController() throws SQLException {
        //dao.readPersons();
    }

    @RequestMapping("/")
    public String home() {
        return "Pizza root";
    }

    @RequestMapping("/order")
    public ArrayList<Pizzaorder> showOrders() {

        ArrayList<Pizzaorder> returnOrders = new ArrayList<Pizzaorder>();


        try {
            returnOrders = dao.getAllOrders();
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return returnOrders;
    }
    @CrossOrigin
    @RequestMapping("/customer")
    public ArrayList<Customer> showCustomers() {

        ArrayList<Customer> returnCustomers = new ArrayList<Customer>();


        try {
            returnCustomers = dao.getAllCustomers();
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return returnCustomers;
    }


    @CrossOrigin
    @RequestMapping("/pizza")
    public ArrayList<Pizza> showPizzas() {

        ArrayList<Pizza> returnPizzas = new ArrayList<Pizza>();


        try {
            returnPizzas = dao.getAllPizzas();
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return returnPizzas;
    }

    @CrossOrigin
    @PostMapping("/pizza")
    public String addPizza(
            @RequestParam(name = "oId", required = true, defaultValue = "") String orderId,
            @RequestParam(name = "pId", required = true, defaultValue = "") String pizzaId,
            @RequestParam(name = "lg", required = false, defaultValue = "false") String isLarge,
            @RequestParam(name = "ec", required = false, defaultValue = "false") String extraCheese,
            @RequestParam(name = "em", required = false, defaultValue = "false") String extraMeat
    ) {
        System.out.println("Adding pizza ID: " + Integer.parseInt(orderId));
        int _pizzaId = -1;
        try {
            _pizzaId = dao.addPizza(Integer.parseInt(orderId), Integer.parseInt(pizzaId), Boolean.parseBoolean(isLarge), Boolean.parseBoolean(extraCheese), Boolean.parseBoolean(extraMeat));
            return String.valueOf(_pizzaId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return String.valueOf(_pizzaId);
    }


    //    public int addCustomer(String first_name, String last_name, String address) {
    @CrossOrigin
    @PostMapping("/customer")
    public String addCustomer(@RequestParam(name = "firstname", required = true, defaultValue = "") String firstname,
                             @RequestParam(name = "lastname", required = true, defaultValue = "") String lastname,
                             @RequestParam(name = "address", required = true, defaultValue = "") String address
    ) {
        System.out.println("Adding customer");
        int customerId = -1;
        try {
            customerId = dao.addCustomer(firstname, lastname, address);
            return String.valueOf(customerId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return String.valueOf(customerId);
    }
    @CrossOrigin
    @PostMapping("/order")
    public String addOrder(
            @RequestParam(name = "customerId", required = true, defaultValue = "0") String customerId) {
        int _customerId = -1;
        System.out.println("Adding Order");
        try {
            _customerId = Integer.parseInt(customerId);
            int phoneId = dao.addPizzaorder(_customerId);
            return String.valueOf(phoneId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "-1";
    }
    @CrossOrigin
    @PostMapping("/submitorder/{ID}")
    public String submitOrder(@PathVariable String ID){
        int _orderId = -1;
        System.out.println("Adding Order");
        try {
            _orderId = Integer.parseInt(ID);
            int status = dao.submitOrder(_orderId);
            return String.valueOf(status);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "-1";
    }


    @CrossOrigin
    @GetMapping("/order/{ID}")
    public OrderInformation personGet(@PathVariable String ID) {
        System.out.println("Trying to find order with ID: " + ID);
        OrderInformation returnOrderInfo = null;

        try {
            returnOrderInfo = dao.getOrderInformation(Integer.parseInt(ID));
            System.out.println("Found person:" + returnOrderInfo.toString());
        } catch (Exception e) {
            System.out.println("Not found!");
            System.out.println(e);
        }
        return returnOrderInfo;
    }

    @CrossOrigin
    @GetMapping("/menu")
    public ArrayList<PizzaMenuItem> showMenu() {

        ArrayList<PizzaMenuItem> menuItems = new ArrayList<PizzaMenuItem>();


        try {
            menuItems = dao.getMenu();
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return menuItems;
    }

    @CrossOrigin
    @GetMapping("/menu/{ID}")
    public PizzaMenuItem getMenuItemById(@PathVariable String ID) {
        PizzaMenuItem returnMenuItem = null;

        try {
            returnMenuItem = dao.pizzaMenu.get(Integer.parseInt(ID)-1);
            System.out.println("MenuItem person:" + returnMenuItem.toString());
        } catch (Exception e) {
            System.out.println("Not found!");
            System.out.println(e);
        }
        return returnMenuItem;
    }

    @CrossOrigin
    @DeleteMapping("/pizza/{ID}")
    public int deletePizza(@PathVariable String ID) {
        int status = dao.deletePizzaById(Integer.parseInt(ID));

        return status;
    }
    @CrossOrigin
    @DeleteMapping("/order/{ID}")
    public int deleteOrder(@PathVariable String ID) {
        int status = dao.deleteOrderById(Integer.parseInt(ID));

        return status;
    }

}
